#!/usr/bin/env python3

import os
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "..", "repos", "mesa_ci", "build_support"))
from build_support import build
from builders import MesonBuilder
from export import Export
from options import Options
from project_map import ProjectMap
from utils.command import rmtree
from utils.utils import git_clean


class MesaPerf:
    def __init__(self):
        self.opts = Options()

        if self.opts.config == 'debug':
            print("ERROR: perf not supported for debug")
            assert(False)

        pm = ProjectMap()
        self._src_dir = pm.project_source_dir("mesa")
        self._build_dir = self._src_dir + "/" + '_'.join(['build', 'bsw'])
        self._install_dir = ("/tmp/build_root/" + self.opts.arch +
                             "/bsw/usr/local")

        # generated with `gcc -E -v -march=native - < /dev/null 2>&1 | grep cc1`
        self._flags = ['-march=silvermont', '-mmmx', '-mno-3dnow', '-msse',
                       '-msse2', '-msse3', '-mssse3', '-mno-sse4a', '-mcx16',
                       '-msahf', '-mmovbe', '-maes', '-mno-sha', '-mpclmul',
                       '-mpopcnt', '-mno-abm', '-mno-lwp', '-mno-fma',
                       '-mno-fma4', '-mno-xop', '-mno-bmi', '-mno-bmi2',
                       '-mno-tbm', '-mno-avx', '-mno-avx2', '-msse4.2',
                       '-msse4.1', '-mno-lzcnt', '-mno-rtm', '-mno-hle',
                       '-mrdrnd', '-mno-f16c', '-mno-fsgsbase', '-mno-rdseed',
                       '-mprfchw', '-mno-adx', '-mfxsr', '-mno-xsave',
                       '-mno-xsaveopt', '-mno-avx512f', '-mno-avx512er',
                       '-mno-avx512cd', '-mno-avx512pf', '-mno-prefetchwt1',
                       '-mno-clflushopt', '-mno-xsavec', '-mno-xsaves',
                       '-mno-avx512dq', '-mno-avx512bw', '-mno-avx512vl',
                       '-mno-avx512ifma', '-mno-avx512vbmi', '-mno-clwb',
                       '-mno-mwaitx', '-mno-clzero', '-mno-pku', '--param',
                       'l1-cache-size=24', '--param', 'l1-cache-line-size=64',
                       '--param', 'l2-cache-size=1024', '-mtune=generic']
        self._flags = " ".join(self._flags)

    def build(self):

        flags = " ".join(self._flags)
        flags = ["CFLAGS=-O2 " + flags,
                 "CXXFLAGS=-O2 " + flags,
                 "CC=ccache gcc",
                 "CXX=ccache g++"]

        options = [
            '-Dgallium-drivers=',
            '-Ddri-drivers=i965,i915',
            '-Dvulkan-drivers=intel',
            '-Dplatforms=x11,drm',
            '-Dbuildtype=release',
            '-Db_ndebug=true',
        ]
        b = MesonBuilder(extra_definitions=options, install=True,
                         cpp_args=self._flags,
                         build_dir=self._build_dir,
                         install_dir=self._install_dir,
                         project='mesa')
        build(b)

        Export().export_perf()

    def clean(self):
        pm = ProjectMap()
        git_clean(pm.project_source_dir("mesa"))
        rmtree(self._build_dir)

    def test(self):
        pass


build(MesaPerf())
