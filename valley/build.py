#!/usr/bin/env python3

import os
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "..", "repos", "mesa_ci", "build_support"))
from build_support import build
from builders import PerfBuilder


build(PerfBuilder("unigine.valley", iterations=1,
                  env={"allow_glsl_extension_directive_midshader":"true",
                       "dual_color_blend_by_location":"true"}))
