#!/usr/bin/env python3

import os
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "..", "repos", "mesa_ci", "build_support"))
from build_support import build
from builders import MesonBuilder
from export import Export
from options import Options
from project_map import ProjectMap
from utils.command import rmtree
from utils.utils import git_clean


class MesaPerf:
    def __init__(self):
        self.opts = Options()

        if self.opts.config == 'debug':
            print("ERROR: perf not supported for debug")
            assert(False)

        pm = ProjectMap()
        self._src_dir = pm.project_source_dir("mesa")
        self._build_dir = self._src_dir + "/" + '_'.join(['build', 'skl'])
        self._install_dir = ("/tmp/build_root/" + self.opts.arch +
                             "/skl/usr/local")

        # generated with `gcc -E -v -march=native - < /dev/null 2>&1 | grep cc1`
        self._flags = ['-march=skylake', '-mmmx',
                       '-mno-3dnow', '-msse', '-msse2',
                       '-msse3', '-mssse3', '-mno-sse4a',
                       '-mcx16', '-msahf', '-mmovbe',
                       '-maes', '-mno-sha', '-mpclmul',
                       '-mpopcnt', '-mabm', '-mno-lwp',
                       '-mfma', '-mno-fma4', '-mno-xop',
                       '-mbmi', '-mbmi2', '-mno-tbm',
                       '-mavx', '-mavx2', '-msse4.2',
                       '-msse4.1', '-mlzcnt', '-mno-rtm',
                       '-mno-hle', '-mrdrnd', '-mf16c',
                       '-mfsgsbase', '-mrdseed', '-mprfchw',
                       '-madx', '-mfxsr', '-mxsave',
                       '-mxsaveopt', '-mno-avx512f',
                       '-mno-avx512er', '-mno-avx512cd',
                       '-mno-avx512pf', '-mno-prefetchwt1',
                       '-mclflushopt', '-mxsavec',
                       '-mxsaves', '-mno-avx512dq',
                       '-mno-avx512bw', '-mno-avx512vl',
                       '-mno-avx512ifma', '-mno-avx512vbmi',
                       '-mno-clwb', '-mno-mwaitx',
                       '-mno-clzero', '-mno-pku', '--param',
                       'l1-cache-size=32', '--param',
                       'l1-cache-line-size=64', '--param',
                       'l2-cache-size=4096',
                       '-mtune=skylake']
        self._flags = " ".join(self._flags)

    def build(self):

        flags = " ".join(self._flags)
        flags = ["CFLAGS=-O2 " + flags,
                 "CXXFLAGS=-O2 " + flags,
                 "CC=ccache gcc",
                 "CXX=ccache g++"]

        options = [
            '-Dgallium-drivers=iris',
            '-Ddri-drivers=i965,i915',
            '-Dvulkan-drivers=intel',
            '-Dplatforms=x11,drm',
            '-Dbuildtype=release',
            '-Db_ndebug=true',
            '-Dllvm=false'
        ]
        b = MesonBuilder(extra_definitions=options, install=True,
                         cpp_args=self._flags,
                         build_dir=self._build_dir,
                         install_dir=self._install_dir,
                         project='mesa')
        build(b)

        Export().export_perf()

    def clean(self):
        pm = ProjectMap()
        git_clean(pm.project_source_dir("mesa"))
        rmtree(self._build_dir)

    def test(self):
        pass


build(MesaPerf())
