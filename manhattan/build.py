#!/usr/bin/env python3

import os
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "..", "repos", "mesa_ci", "build_support"))
from build_support import build
from builders import PerfBuilder
from options import Options


class ManhattanTimeout:
    def __init__(self):
        self._options = Options()

    def GetDuration(self):
        base_time = 20
        if "bsw" in self._options.hardware:
            base_time = base_time * 2
        if self._options.type == "daily":
            base_time = base_time * 5
        return base_time


build(PerfBuilder("gfxbench5.manhattan", iterations=3),
      time_limit=ManhattanTimeout())
